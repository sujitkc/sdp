class Account {
  int Balance = 0;

  public Account(int b) {
    this.Balance = b;
  }

  public synchronized void deposit(int amount) {
    int newAmount = this.Balance + amount;
    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      System.out.println("Deposit interrupted!");
      System.exit(1);
    }
    this.Balance = newAmount;
  }

  public synchronized void withdraw(int amount) {
    int newAmount = this.Balance - amount;
    this.Balance = newAmount;
  }

  public int getBalance () {
    return this.Balance;
  }
}    

public class Bank2 {

  private static class User implements Runnable {
    private final String name;
    private final int amount;
    private Account account;

    public User(String n, int a, Account acc) {
      this.name = n;
      this.amount = a;
      this.account = acc;
    }

    public void run() {
      this.account.deposit(this.amount);
    }  
  }

  public static void main(String[] args) {
    Account account = new Account(1000);
    User[] users = {
      new User("user 1", 100, account),
      new User("user 2", 200, account),
      new User("user 3", 100, account),
      new User("user 4", 200, account)
    };

    int increment = 100 + 200 + 100 + 200;
    int expectedBalance = account.getBalance() + increment;
    for(User u : users) {
      Thread t = new Thread(u);
      t.start();
    }
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      System.out.println("Deposit interrupted!");
      System.exit(1);
    }
    int finalBalance = account.getBalance();
    if(finalBalance == expectedBalance) {
      System.out.println("Success");
    }
    else {
      System.out.println("Error: Expected balance = " + expectedBalance +
        "; Final balance = " + finalBalance);
    }
  }
}
