class Account {
  private int Balance = 0;
  public static final int topupLimit = 500;

  public Account(int b) {
    this.Balance = b;
  }

  private static void delay(int n) {
    try {
      Thread.sleep(n);
    } catch (InterruptedException e) {
      System.out.println("Deposit interrupted!");
      System.exit(1);
    }
  }

  public synchronized void deposit(int amount) {
    int newAmount = this.Balance + amount;
    Account.delay(100);
    this.Balance = newAmount;
  }

  public synchronized void withdraw(int amount) {
    int newAmount = this.Balance - amount;
    this.Balance = newAmount;
  }

  public synchronized void topup(Account dest) {
    int amount = Account.topupLimit - dest.getBalance();
    if(amount > 0) {
      this.withdraw(amount);
      dest.deposit(amount);
    }
  }

  public int getBalance () {
    return this.Balance;
  }
}

public class Bank3 {

  private static class User implements Runnable {
    private final String name;
    private Account account;
    private Account beneficiary;

    public User(String n, Account acc, Account ben) {
      this.name = n;
      this.account = acc;
      this.beneficiary = ben;
    }

    public void run() {
      this.account.topup(this.beneficiary);
    }
  }

  public static void main(String[] args) {
    Account yourAccount = new Account(200);
    User u1 = new User("Mom", new Account(2000), yourAccount);
    User u2 = new User("Dad", new Account(1000), yourAccount);

    new Thread(u1).start();
    new Thread(u2).start();

    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      System.out.println("Interrupted!");
      System.exit(1);
    }
    int finalBalance = yourAccount.getBalance();
    if(finalBalance == Account.topupLimit) {
      System.out.println("Success");
    }
    else {
      System.out.println("Error: Expected balance = " + Account.topupLimit +
        "; Final balance = " + finalBalance);
    }
  }
}  
