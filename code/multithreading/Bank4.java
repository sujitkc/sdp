class Account {
  int Balance = 0;
  final String name;
  public Account(String n, int b) {
    this.name = n;
    this.Balance = b;
  }

  public synchronized void deposit(int amount) {
    System.out.println(amount + " being deposited to " + this.name);
    int newAmount = this.Balance + amount;
    this.Balance = newAmount;
    System.out.println("deposited to " + this.name + " done.");
  }

  public synchronized void withdraw(int amount) {
    System.out.println(amount + " being withdrawn from " + this.name);
    int newAmount = this.Balance - amount;
    this.Balance = newAmount;
    System.out.println("withdrawal from " + this.name + " done.");
  }

  public synchronized void transfer(Account dest, int amount) {
    System.out.println(this.name + " transferring " + amount + " to " + 
      dest.name);
    this.withdraw(amount);
    dest.deposit(amount);
    System.out.println(this.name + " transferring " + amount + " to " +
      dest.name + ": done.");
  }

  public int getBalance () {
    return this.Balance;
  }
}

public class Bank4 {

  private static class User implements Runnable {
    private final String name;
    private final int amount;
    private Account account;
    private Account beneficiary;
    
    public User(String n, int a, Account acc, Account ben) {
      this.name = n;
      this.amount = a;
      this.account = acc;
      this.beneficiary = ben;
    }

    public boolean transferDone = false;
    public void run() {
      transferDone = false;
      this.account.transfer(this.beneficiary, this.amount);
      transferDone = true;
    }  
  }

  public static void main(String[] args) {
    int myBalance = 1000;
    int dadsBalance = 20000;
    int myAmount = 100;
    int dadsAmount = 200;
    Account myAccount = new Account("My Account", myBalance);
    Account dadsAccount = new Account("Dad's Account", dadsBalance);
    User me  = new User("me", myAmount, myAccount, dadsAccount);
    User dad = new User("dad", dadsAmount, dadsAccount, myAccount);

    int myExpectedBalance = myAccount.getBalance() + dadsAmount - myAmount;
    int dadsExpectedBalance = dadsAccount.getBalance() + myAmount - dadsAmount;
    Thread meThread = new Thread(me);
    Thread dadThread = new Thread(dad);
    meThread.start();
    dadThread.start();
    try {
      meThread.join();
      dadThread.join();
    }
    catch(InterruptedException e) {
      e.printStackTrace();
    }
    int myFinalBalance = myAccount.getBalance();
    if(myFinalBalance == myExpectedBalance) {
      System.out.println("Success");
    }
    else {
      System.out.println("Error: My Expected balance = " + myExpectedBalance +
        "; My Final balance = " + myFinalBalance);
    }
    int dadsFinalBalance = dadsAccount.getBalance();
    if(dadsFinalBalance == dadsExpectedBalance) {
      System.out.println("Success");
    }
    else {
      System.out.println("Error: Dad's Expected balance = " + dadsExpectedBalance +
        "; Dad's Final balance = " + dadsFinalBalance);
    }
  }
}
