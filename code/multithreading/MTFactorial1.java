public class MTFactorial1 {
  private static Factorial[] factorials = {
    new Factorial(1),
    new Factorial(2),
    new Factorial(3),
    new Factorial(4),
    new Factorial(5)
  };


  public static void main(String[] args) {
    int sum = sumAll(factorials);
    System.out.println("Sum of factorials = " + sum);
  }

  public static int sumAll(Factorial[] factorials) {
    int sum = 0;
    new Thread(factorials[0]).start();
    new Thread(factorials[1]).start();
    new Thread(factorials[2]).start();
    new Thread(factorials[3]).start();
    new Thread(factorials[4]).start();
    try {
      Thread.sleep(1000);
    } catch(Exception e) {
    }
    for(Factorial f : factorials) {
      sum += f.getResult();
    }
    return sum;
  }
}

class Factorial implements Runnable {
  int num;
  int result;
  public boolean done = false;
  public Factorial(int n) {
    this.num = n;
  }

  public void run() {
    synchronized(this) {
      result = this.factorial(this.num);
      System.out.println("Factorial " + this.num + " = " + result);
      this.done = true;
      this.notifyAll();
    }
  }

  public int factorial(int n) {
    if(n == 1) {
      return 1;
    }
    else {
      return n * this.factorial(n - 1);
    }
  }

  public int getResult() {
    return this.result;
  }
}
