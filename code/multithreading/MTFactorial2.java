public class MTFactorial2 {
  private static Factorial[] factorials = {
    new Factorial(1),
    new Factorial(2),
    new Factorial(3),
    new Factorial(4),
    new Factorial(5)
  };


  public static void main(String[] args) {
    int sum = sumAll(factorials);
    System.out.println("Sum of factorials = " + sum);
  }

  public static int sumAll(Factorial[] factorials) {
    int sum = 0;
    try{
      new Thread(factorials[0]).start();
      new Thread(factorials[1]).start();
      new Thread(factorials[2]).start();
      new Thread(factorials[3]).start();
      new Thread(factorials[4]).start();
      synchronized(factorials[0]) {
        if(factorials[0].result == 0) {
          factorials[0].wait();
        }
      }
      synchronized(factorials[1]) {
        if(factorials[1].result == 0) {
          factorials[1].wait();
        }
      }
      synchronized(factorials[2]) {
        if(factorials[2].result == 0) {
          factorials[2].wait();
        }
      }
      synchronized(factorials[3]) {
        if(factorials[3].result == 0) {
          factorials[3].wait();
        }
      }
      synchronized(factorials[4]) {
        if(factorials[4].result == 0) {
          factorials[4].wait();
        }
      }
    } catch(InterruptedException e){
      e.printStackTrace();
           }
    for(Factorial f : factorials) {
      sum += f.getResult();
    }
    return sum;
  }
}

class Factorial implements Runnable {
  int num;
  public int result = 0;
  public Factorial(int n) {
    this.num = n;
  }

  public void run() {
    synchronized(this) {
      result = this.factorial(this.num);
      System.out.println("Factorial " + this.num + " = " + result);
      this.notify();
    }
  }

  public int factorial(int n) {
    if(n == 1) {
      return 1;
    }
    else {
      return n * this.factorial(n - 1);
    }
  }

  public int getResult() {
    return this.result;
  }
}
