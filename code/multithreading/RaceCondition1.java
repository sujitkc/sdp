class Writer extends Thread {
  public static final Object LOCK = new Object();
  public void compute() {
    synchronized(Writer.LOCK) {
    int number = RaceCondition1.number;
    System.out.println(" = " + number);
    try {
      Thread.sleep(10);
    }
    catch(InterruptedException e) {
      e.printStackTrace();
    }
    number += 1;
    RaceCondition1.number = number;
    }
  }

  public void run() {
    this.compute();
  }
}

class RaceCondition1 {
  public static volatile int number = 0;
  public static void main(String[] args) {
    Writer writer1 = new Writer();
    Writer writer2 = new Writer();

    writer1.start();
    writer2.start();

    try {
      writer1.join();
      writer2.join();
    }
    catch(InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("number = " + RaceCondition1.number);
  }
}

