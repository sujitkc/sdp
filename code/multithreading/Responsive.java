import java.util.Scanner;

class Job implements Runnable {
  public void doJob() {
    System.out.println("Doing something time consuming and dangerous ...");
    try {
      Thread.sleep(10000);
    }
    catch(InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void anotherMethod() {
    System.out.println("Executing another method ...");
  }

  public void run() {
    this.doJob();
  }
}

public class Responsive {

  public static void main(String[] args) {
    Job job = new Job();
    Thread thread = new Thread(new Job());
    thread.start();
    job.anotherMethod();
    Scanner scan= new Scanner(System.in);
    System.out.print("Press Enter");
    String text= scan.nextLine();
    System.out.println("Done!");
    System.exit(0);
    
  }
}
