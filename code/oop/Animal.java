public class Animal {

  public static void main(String[] args) {

    System.out.println("Hello world!");

    Dog myDog = new Dog("Kutta", "Me"); // sadda kutta kutta
    Dog yourDog = new Dog("Toamy", "You"); // Tuada kutta toamy

    System.out.println(myDog.getName());
    System.out.println(yourDog.getName());

    myDog.setOwner("You");
    System.out.println(myDog.getOwner());
    System.out.println(yourDog.getOwner());

    System.out.println(myDog.makeSound());

    Dog orphanDog = new Dog("Sheru");
    System.out.println(orphanDog.getOwner());
    System.out.println(orphanDog);

    Cat myCat = new Cat("Kitty", "Me");
    System.out.println(myCat);

    DomesticatedAnimal a = new Dog("Pluto", "Mickey");
    System.out.println(a);

    a = new Cat("Tom", "Jerry");
    System.out.println(a);
  }
}

abstract class DomesticatedAnimal {
  private final String name;
  private String owner;

  public DomesticatedAnimal(String name, String owner) {
    this.name = name;
    this.owner = owner;
  }

  public DomesticatedAnimal(String name) {
    // this.name = name;
    // this.owner = "Municipality";

    this(name, "Wife");
  }
  // getter method
  public String getName() {
    return this.name;
  }

  public String getOwner() {
    return this.owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public abstract String makeSound();
/*
  public String makeSound() {
    return "Yes, my wife!";
  }
*/
  public String makeSound(String sound) {
    return sound;
  }

  public String toString() {
    return "Husband (" + this.name + ", " + this.owner + ")";
  }
}


// class definition is a way to add user defined types.
class Dog extends DomesticatedAnimal {
//  private final String name;
//  private String owner;

  public Dog(String name, String owner) {
    super(name, owner);
//    this.name = name;
//    this.owner = owner;
  }

  public Dog(String name) {
    // this.name = name;
    // this.owner = "Municipality";

    this(name, "Municipality");
  }

  /*
  // getter method
  public String getName() {
    return this.name;
  }

  public String getOwner() {
    return this.owner;
  }
  */

  public void setOwner(String owner) {
    if(!owner.equals("Kitty")) {
      super.setOwner(owner);
    }
  }

  public String makeSound() {
    return "Bhow Bhow!";
  }

  public String makeSound(String sound) {
    return sound;
  }

  public String toString() {
    return "Dog (" + this.getName() + ", " + this.getOwner() + ")";
  }
}

class Cat extends DomesticatedAnimal {
  private final String name;
  private String owner;

  public Cat(String name, String owner) {
    super(name, owner);
    this.name = name;
    this.owner = owner;
  }

  public Cat(String name) {
    // this.name = name;
    // this.owner = "Municipality";

    this(name, "Municipality");
  }
  // getter method
  public String getName() {
    return this.name;
  }

  public String getOwner() {
    return this.owner;
  }

  public void setOwner(String owner) {
    if(!owner.equals("Kitty")) {
      this.owner = owner;
    }
  }

  public String makeSound() {
    return "Prrr Prrr!";
  }

  public String makeSound(String sound) {
    return sound;
  }

  public String toString() {
    return "Cat (" + this.name + ", " + this.owner + ")";
  }
}
